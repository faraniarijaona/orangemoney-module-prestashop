﻿<?php

/**
 *  @author    Fety Faraniarijaona <ffaraniarijaona@gmail.com>
 */

class OrangemoneyRetouModuleFrontController extends ModuleFrontController {
    /**
     * Retours de l'api de paiement
     */
    public function postProcess()
    {
        //Vérification générales 
        $cart = $this->context->cart;
        $authorized = false;

        /*
         * Verify if this module is enabled and if the cart has
         * a valid customer, delivery address and invoice address
         */
        if (!$this->module->active || $cart->id_customer == 0 || $cart->id_address_delivery == 0
            || $cart->id_address_invoice == 0) {
            Tools::redirect('index.php?controller=order&step=1');
        }

        /** 
         * Verify if this payment module is authorized
         */
        foreach (Module::getPaymentModules() as $module) {
            if ($module['name'] == 'orangemoney') {
                $authorized = true;
                break;
            }
        }
        
        /** @var CustomerCore $customer */
        $customer = new Customer($cart->id_customer);

        /**
         * Check if this is a vlaid customer account
         */
        if (!Validate::isLoadedObject($customer)) {
            Tools::redirect('index.php?controller=order&step=1');
        }
        /**
         * check pay token
         */
        if(!$this->context->cookie->__isset('ff_pay_token')){
            Tools::redirect('index.php?controller=order&step=1');
        }

        //GET STATUS By ORDER ID
        $status = json_decode($this->getStatus());

        if($status->{'status'} != "SUCCESS"){
            Tools::redirect('index.php?controller=order&step=1');
            return;
        }

        $this->context->cookie->__unset('ff_pay_token'); //remove pay_token for security end

        /**
         * Place the order
         */
        $this->module->validateOrder(
            (int) $this->context->cart->id,
            Configuration::get('PS_OS_PAYMENT'),
            (float) $this->context->cart->getOrderTotal(true, Cart::BOTH),
            $this->module->displayName.", Ref: ".$status->{'txnid'},
            "OrangeMoney TxnID : ".$status->{'txnid'},
            null,
            (int) $this->context->currency->id,
            false,
            $customer->secure_key
        );

        /**
         * Redirect the customer to the order confirmation page
         */
        Tools::redirect('index.php?controller=order-confirmation&id_cart='.(int)$cart->id.'&id_module='.(int)$this->module->id.'&id_order='.$this->module->currentOrder.'&key='.$customer->secure_key);
    }   

    private function getStatus(){

        $pay_token = $this->context->cookie->__get('ff_om_pay_token');

        $_order_id = $this->context->cookie->__get('ff_om_order_id');

        $amount = $this->context->cookie->__get('ff_om_amount');

        $DATA = array('amount'=>$amount, 'order_id'=> $_order_id, 'pay_token'=>$pay_token);

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, Configuration::get('TRANSACTION_STATUS_URL'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($DATA));

        $headers = array();
        $headers[] = 'Authorization: '.Configuration::get('ACCESS_TOKEN');
        $headers[] = 'Accept: application/json';
        $headers[] = 'Content-Type: application/json';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);

        return $result;

    }
}
