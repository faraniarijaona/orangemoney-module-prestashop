<?php

/**
 *  @author    Fety Faraniarijaona <ffaraniarijaona@gmail.com>
 */

class OrangemoneyAnnulationModuleFrontController extends ModuleFrontController {
	/**
     * Retours de l'api de paiement
     */
    public function postProcess()
    {
        Tools::redirect('index.php?controller=order&step=1');
    }   
}
