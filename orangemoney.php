<?php


/**
 *  @author    Fety Faraniarijaona <ffaraniarijaona@gmail.com>
 */

use PrestaShop\PrestaShop\Core\Payment\PaymentOption;

if (!defined('_PS_VERSION_')) {
  exit;
}

class Orangemoney  extends PaymentModule
{
  protected $_html;

  public function __construct()
  {
    $this->name = 'orangemoney';
    $this->tab = 'payments_gateways';
    $this->version = '0.1.0';
    $this->author = 'Fety Faraniarijaona';
    $this->bootstrap = true;

    parent::__construct();

    $this->displayName = $this->l('OrangeMoney');
    $this->description = $this->l('Accepter Paiement par OrangeMoney');
    $this->ps_versions_compliancy = array('min' => '1.7.0', 'max' => _PS_VERSION_);
  }

  public function install()
  {
    if (!parent::install() 
      || !$this->registerHook('paymentOptions')
      || !$this->registerHook('paymentReturn')
    ) {
      return false;
  }
  return true;
}

    /**
    * GET PAYMENT URL
    */
    private function getPaymentUrl(){

      if(empty(Configuration::get('ACCESS_TOKEN')))
        $this->refreshAccesTooken();

      $_order_id = $this->context->cart->id."0".time();

      $DATA = array(
        'merchant_key' => Configuration::get('MERCHANT_KEY'),
        'currency' => Configuration::get('CURRENCY'),
        'order_id' => $_order_id ,
        'amount' =>  $this->context->cart->getOrderTotal(true, Cart::BOTH),
        'return_url' => Configuration::get('RETURN_URL'),
        'cancel_url' => Configuration::get('CANCEL_URL'),
        'notif_url' => Configuration::get('RETURN_URL'),
        'lang' => Configuration::get('LANG'),
        'reference' => "PAIEMENT N ".$this->context->cart->id
      );

      $ch = curl_init();

      curl_setopt($ch, CURLOPT_URL, Configuration::get('BASE_URL'));
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($DATA));

      $headers = array();
      $headers[] = 'Authorization: '.Configuration::get('ACCESS_TOKEN');
      $headers[] = 'Accept: application/json';
      $headers[] = 'Content-Type: application/json';
      curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

      $result = curl_exec($ch);

      $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
      
      $result = json_decode($result);
      curl_close($ch);

      if($result->{'status'} == 201 ){

        //ADD PAY_TOKEN IN COOKIES
        $this->context->cookie->__set('ff_om_pay_token', $result->{'pay_token'});
        $this->context->cookie->write();

        $this->context->cookie->__set('ff_om_order_id', $_order_id);
        $this->context->cookie->write();

        $this->context->cookie->__set('ff_om_amount', $this->context->cart->getOrderTotal(true, Cart::BOTH));
        $this->context->cookie->write();


        return $result;
      }

      if($code != 401)
        return -1;

      $result = $this->refreshAccesTooken();

      if($result == -1)
        return -1;

      return $this->getPaymentUrl();
    }

    private function refreshAccesTooken(){
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, Configuration::get('OAUTH_URL'));
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, "grant_type=client_credentials");
      $headers = array();
      $headers[] = 'Authorization: '. Configuration::get('CONSUMER_KEY');
      $headers[] = 'Content-Type: application/x-www-form-urlencoded';
      curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

      $result = curl_exec($ch);
      if (curl_errno($ch)) {
        return -1;
      }
      curl_close($ch);
      $result = json_decode($result);
      Configuration::updateValue('ACCESS_TOKEN',$result->{'token_type'}.' '. $result->{'access_token'});
      $date_end = new DateTime();
      $date_end->add(new DateInterval('PT'.$result->{'expires_in'}.'S'));
      Configuration::updateValue('TOKEN_EXPIRE', $date_end->format('j M Y H:i'));

      return $result;
    }

    public function hookPaymentOptions($params){
      
      if (!$this->active) {
        return;
      }

      $parameters = $this->getPaymentUrl();

      if($parameters == -1)
        return ;

      $this->smarty->assign(['payment_url' =>$parameters->{'payment_url'}]);

      $apiPayement = new PaymentOption();
      $apiPayement->setModuleName($this->name)
      ->setCallToActionText($this->l('Orange Money WEBPAY Madagascar'))
                //Définition d'un formulaire personnalisé
      ->setForm($this->fetch('module:orangemoney/views/templates/hook/payment_api_form.tpl'))
      ->setAdditionalInformation($this->fetch('module:orangemoney/views/templates/hook/displayPaymentApi.tpl'));

      return [$apiPayement];

    }

    /**
     * Affichage du message de confirmation de la commande
     * @param type $params
     * @return type
     */
    public function hookDisplayPaymentReturn($params) 
    {
      if (!$this->active) {
        return;
      }

      $this->smarty->assign(
        $this->getTemplateVars()
      );
      return $this->fetch('module:orangemoney/views/templates/hook/payment_return.tpl');
    }

    /**
     * Configuration admin du module
     */
    public function getContent()
    {
      $this->_html .=$this->postProcess();
      $this->_html .= $this->renderForm();

      return $this->_html;

    }

    /**
     * Traitement de la configuration BO
     * @return type
     */
    public function postProcess()
    {
      if ( Tools::isSubmit('SubmitPaymentConfiguration'))
      {
        Configuration::updateValue('OAUTH_URL', Tools::getValue('OAUTH_URL'));
        Configuration::updateValue('BASE_URL', Tools::getValue('BASE_URL'));
        Configuration::updateValue('CONSUMER_KEY', Tools::getValue('CONSUMER_KEY'));
        Configuration::updateValue('MERCHANT_KEY', Tools::getValue('MERCHANT_KEY'));
        Configuration::updateValue('ACCESS_TOKEN', Tools::getValue('ACCESS_TOKEN'));
        Configuration::updateValue('TOKEN_EXPIRE', Tools::getValue('TOKEN_EXPIRE'));
        Configuration::updateValue('CURRENCY', Tools::getValue('CURRENCY'));
        Configuration::updateValue('TRANSACTION_STATUS_URL', Tools::getValue('TRANSACTION_STATUS_URL'));
        Configuration::updateValue('CANCEL_URL', Tools::getValue('CANCEL_URL'));
        Configuration::updateValue('RETURN_URL', Tools::getValue('RETURN_URL'));
        Configuration::updateValue('LANG', Tools::getValue('LANG'));
      }
      return $this->displayConfirmation($this->l('Configuration mise à jour!'));
    }

    /**
    * Formulaire de configuration admin
    */
    public function renderForm()
    {
     $fields_form = [
       'form' => [
         'legend' => [
           'title' => $this->l('Configuration paiement OrangeMoney'),
           'icon' => 'icon-cogs'
         ],
         'description' => $this->l('Ici, vous configurez le paiement par OrangeMoney'),
         'input' => [
          [
           'type' => 'text',
           'label' => $this->l('URL pour authentifiaction OAUTH'),
           'name' => 'OAUTH_URL',
           'required' => true,
           'empty_message' => $this->l('Completez URL pour authentifiaction')
         ],
         [
           'type' => 'text',
           'label' => $this->l("Authorization Key (CONSUMER KEY)"),
           'name' => 'CONSUMER_KEY',
           'required' => true,
           'empty_message' => $this->l("Completez l'Authorization")
         ],
         [
           'type' => 'text',
           'label' => $this->l('Transaction Initialization URL'),
           'name' => 'BASE_URL',
           'required' => true,
           'empty_message' => $this->l('Completez la Base URL')
         ],
         [
           'type' => 'text',
           'label' => $this->l("Transaction Status URL"),
           'name' => 'TRANSACTION_STATUS_URL',
           'required' => true,
           'empty_message' => $this->l("Remplir l'URL pour vérifier le statut de transaction")
         ],
         [
           'type' => 'text',
           'label' => $this->l('Token'),
           'name' => 'ACCESS_TOKEN',
           'disabled' => true,
           'empty_message' => $this->l("Ce champ va être completer automatiquement")
         ],
         [
           'type' => 'text',
           'label' => $this->l("Date d'Expiration du token"),
           'name' => 'TOKEN_EXPIRE',
           'disabled' => true,
           'empty_message' => $this->l("Ce champ va être completer automatiquement")
         ],
         [
           'type' => 'text',
           'label' => $this->l('Clé du marchant (MERCHANT KEY)'),
           'name' => 'MERCHANT_KEY',
           'required' => true,
           'empty_message' => $this->l("Completez la Clé du Marchand")
         ],
         [
           'type' => 'text',
           'label' => $this->l("Devise"),
           'name' => 'CURRENCY',
           'required' => true,
           'empty_message' => $this->l("Remplir Devise")
         ],
         [
           'type' => 'text',
           'label' => $this->l("URL de retour"),
           'name' => 'RETURN_URL',
           'required' => true,
           'empty_message' => $this->l("Remplir URL de retour")
         ],
         [
           'type' => 'text',
           'label' => $this->l("URL d'annulation"),
           'name' => 'CANCEL_URL',
           'required' => true,
           'empty_message' => $this->l("Remplir URL d'abandon")
         ],
         [
           'type' => 'text',
           'label' => $this->l("Langue (mettre fr pour français)"),
           'name' => 'LANG',
           'required' => true,
           'empty_message' => $this->l("Remplir la langue")
         ]
       ],
       'submit' => [
         'title' => $this->l('Enregistrer'),
         'class' => 'button btn btn-default pull-right'
       ]
     ]
   ];

   $helper = new HelperForm();
   $helper->show_toolbar = false;
   $helper->default_form_language = (int)Configuration::get('PS_LANG_DEFAULT');
   $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
   $helper->id = 'orangemoney';
   $helper->identifier = 'orangemoney';
   $helper->submit_action = 'SubmitPaymentConfiguration';
   $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
   $helper->token = Tools::getAdminTokenLite('AdminModules');
   $helper->tpl_vars = [
     'fields_value' => $this->getConfigFieldsValues(),
     'languages' => $this->context->controller->getLanguages(),
     'id_language' => $this->context->language->id
   ];

   return $helper->generateForm(array($fields_form));
 }

    /**
     * Récupération des variables de configuration du formulaire admin
     */
    public function getConfigFieldsValues()
    {
      return [
        'OAUTH_URL' => Tools::getValue('OAUTH_URL', Configuration::get('OAUTH_URL')),
        'BASE_URL' => Tools::getValue('BASE_URL', Configuration::get('BASE_URL')),
        'CONSUMER_KEY' => Tools::getValue('CONSUMER_KEY', Configuration::get('CONSUMER_KEY')),
        'MERCHANT_KEY' => Tools::getValue('MERCHANT_KEY', Configuration::get('MERCHANT_KEY')),
        'ACCESS_TOKEN' => Tools::getValue('ACCESS_TOKEN', Configuration::get('ACCESS_TOKEN')),
        'TOKEN_EXPIRE' => Tools::getValue('TOKEN_EXPIRE', Configuration::get('TOKEN_EXPIRE')),
        'CURRENCY' => Tools::getValue('CURRENCY', Configuration::get('CURRENCY')),
        'TRANSACTION_STATUS_URL' => Tools::getValue('TRANSACTION_STATUS_URL', Configuration::get('TRANSACTION_STATUS_URL')),
        'CANCEL_URL' => Tools::getValue('CANCEL_URL', Configuration::get('CANCEL_URL')),
        'RETURN_URL' => Tools::getValue('RETURN_URL', Configuration::get('RETURN_URL')),
        'LANG' => Tools::getValue('LANG', Configuration::get('LANG'))
      ];
    }

    /**
     * Récupération des informations du template
     * @return array
     */
    public function getTemplateVars()
    {
      return [
        'shop_name' => $this->context->shop->name,
        'custom_var' => $this->l('My custom var value'),
        'payment_details' => $this->l('custom details'),
      ];
    }

  }