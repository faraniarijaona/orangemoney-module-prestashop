## OrangeMoney for Prestashop
Orange Money web payment module for Prestashop by [@faraniarijaona][1] .

Supported PS version : >= 1.7

*<u>Installation</u>*
1.	clone this repo
2.	zip it under `orangemoney.zip` 
3.	Go to Module Manager in the prestashop admin
4.	install module
*(alternative for step 1. and 2. : download directly the zip format from repo, then rename it from orangemoney-module-prestashop-master.zip to orangemoney.zip)*
5. Configure it :
![Orange Money module configuration](https://i.ibb.co/rQzr7xM/om-conf.png)

As return URL : ```{your_base_url}/module/orangemoney/retour``` or ```{your_base_url}/module/orangemoney/notification```

As cancel URL : ```{your_base_url}/module/orangemoney/annulation```

 
In order to get above information, you have to see on [https://developer.orange.com](https://developer.orange.com) contact [orange money][2] commercial service and you have to be a recognized organization with a legal notice.

*<u>want to talk about it or  integrate it to another technologie and CMS?</u>*
contact me by mail : [ffaraniarijaona@gmail.com](mailto:ffaraniarijaona@gmail.com?subject=mvola%20integration)
or [![Gitter](https://badges.gitter.im/faraniarijaona-orangemoney-module-prestashop/community.svg)](https://gitter.im/faraniarijaona-orangemoney-module-prestashop/community?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge)


Thanks!!!

## Reference
[https://shopin.mg](https://shopin.mg)

[1]:https://gitlab.com/faraniarijaona
[2]:https://orange.mg
